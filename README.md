# simple-multi-vlan-aware-bridge

## A Layer 2 configuration to use multiple VLAN aware bridges to isolate VLAN traffic across inter-switch links.

This repository contains the configuration files for a simple L2 topology with two switches and eight servers. The idea is that server pairs may only communicate with each other across the interswitch link. There is no VLAN routing.

For example, these server pairs may only communicate with each other:
- ubuntu0 - ubuntu4 - VLAN 10
- ubuntu1 - ubuntu5 - VLAN 20
- ubuntu2 - ubuntu6 - VLAN 30
- ubuntu3 - ubuntu7 - VLAN 40

Futhermore, VLANs 10 and 20 are transported across the `swp5` inter-switch links and VLANs 30 and 40 are transported across the `swp6` link. 

The NVUE `startup.yaml` files for the Cumulus Linux switches are stored in the `switches`. The `config.yaml` Netplan configurations for the Ubuntu servers are stored in the `servers` directory.

## Want to try it out?
This example was built in [NVIDIA Air](http://air.nvidia.com). The `multi_vab.json` file can be loaded into the Air simulation builder tool to create the topology and build the simulation.

The topology of the environment looks like this:

![topology](multi_vab.png)

